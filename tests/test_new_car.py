from src.new_car import Car


def test_if_new_car_has_course_0():
    test_new_car_course = Car("Skoda Fabia", 2004)
    assert test_new_car_course.course == 0


def test_if_new_car_has_course_100():
    test_new_car_course = Car("Skoda Fabia", 2004)
    test_new_car_course.increase_course_with_driven_distance(100)
    assert test_new_car_course.course == 100


def test_if_new_car_has_course_300():
    test_new_car_course = Car("Skoda Fabia", 2004)
    test_new_car_course.increase_course_with_driven_distance(100)
    test_new_car_course.increase_course_with_driven_distance(200)
    assert test_new_car_course.course == 300


def test_if_car_has_warranty_2020():
    test_car_warranty = Car("Opel Corsa", 2020)
    test_car_warranty.increase_course_with_driven_distance(119000)
    assert test_car_warranty.check_if_car_has_warranty()


def test_if_car_has_warranty_2007():
    test_car_warranty = Car("Fiat Panda", 2007)
    test_car_warranty.increase_course_with_driven_distance(19000)
    assert not test_car_warranty.check_if_car_has_warranty()


def test_if_description_is_correct():
    test_car_description = Car("Honda Civic", 1998)
    test_car_description.increase_course_with_driven_distance(230000)
    assert test_car_description.get_car_description() == 'This is a Honda Civic made in 1998. Currently it drove 230000 kilometers'
